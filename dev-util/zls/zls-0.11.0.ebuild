# Copyright 2001-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit edo

DESCRIPTION="The officially unofficial Ziglang language server"
HOMEPAGE="https://github.com/zigtools/zls"

KNOWN_FOLDERS="a564f582122326328dad6b59209d070d57c4e6ae"
DIFFZ="90353d401c59e2ca5ed0abe5444c29ad3d7489aa"
BINNED_ALLOCATOR="6c3321e0969ff2463f8335da5601986cf2108690"

SRC_URI="
	https://github.com/zigtools/zls/archive/refs/tags/$PV.tar.gz -> $PN-$PV.tar.gz
	https://github.com/ziglibs/known-folders/archive/${KNOWN_FOLDERS}.tar.gz -> known-folders-${KNOWN_FOLDERS}.tar.gz
	https://github.com/ziglibs/diffz/archive/${DIFFZ}.tar.gz -> diffz-${DIFFZ}.tar.gz
	https://gist.github.com/antlilja/8372900fcc09e38d7b0b6bbaddad3904/archive/${BINNED_ALLOCATOR}.tar.gz -> binned_allocator-${BINNED_ALLOCATOR}.tar.gz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64"

ZIG_VERSION="0.11"

DEPEND="|| ( dev-lang/zig:${ZIG_VERSION} dev-lang/zig-bin:${ZIG_VERSION} )"

QA_FLAGS_IGNORED="usr/bin/zls"

src_prepare() {
	default

	mkdir -p "$WORKDIR/cache/p"
	mv "$WORKDIR/known-folders-${KNOWN_FOLDERS}" "$WORKDIR/cache/p/122048992ca58a78318b6eba4f65c692564be5af3b30fbef50cd4abeda981b2e7fa5"
	mv "$WORKDIR/diffz-${DIFFZ}" "$WORKDIR/cache/p/122089a8247a693cad53beb161bde6c30f71376cd4298798d45b32740c3581405864"
	mv "$WORKDIR/8372900fcc09e38d7b0b6bbaddad3904-${BINNED_ALLOCATOR}" "$WORKDIR/cache/p/1220363c7e27b2d3f39de6ff6e90f9537a0634199860fea237a55ddb1e1717f5d6a5"
}

src_configure() {
	export ZIG_GLOBAL_CACHE_DIR=$WORKDIR/cache
}

src_test() {
	edo zig build test -Doptimize=ReleaseSafe -Dpie=true
}

src_install() {
	edo zig build install --prefix /usr -Doptimize=ReleaseSafe -Dpie=true
	dodoc README.md
}
